# Start from this base image
FROM rootproject/root:6.22.06-conda
# Copy the repo
COPY . /fit
# Change working directory
WORKDIR /fit
